package br.com.voo.model.pessoas;


import java.time.LocalDate;
import java.time.LocalDateTime;

public class Passageiro extends Pessoa{

    private LocalDate dataNascimento;
    private double salario;

    @Override
    public double salario() {
        return salario;
    }



    public Passageiro() {
    }
    public Passageiro(String nome, String login, String senha, String email, LocalDateTime dataAcesso, LocalDate dataNascimento, double salario) {
        super(nome, login, senha, email, dataAcesso);
        this.dataNascimento = dataNascimento;
        this.salario = salario;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
