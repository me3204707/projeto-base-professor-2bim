package br.com.voo.model.pessoas;

import java.time.LocalDateTime;

public abstract class Pessoa {

    private String nome, login, senha, email;

    private LocalDateTime dataAcesso;

    public Pessoa() {
    }

    public Pessoa(String nome, String login, String senha, String email, LocalDateTime dataAcesso) {
        this.nome = nome;
        this.login = login;
        this.senha = senha;
        this.email = email;
        this.dataAcesso = dataAcesso;
    }

    public abstract double salario();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getDataAcesso() {
        return dataAcesso;
    }

    public void setDataAcesso(LocalDateTime dataAcesso) {
        this.dataAcesso = dataAcesso;
    }
}
