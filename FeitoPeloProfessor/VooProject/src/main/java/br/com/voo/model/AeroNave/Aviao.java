package br.com.voo.model.AeroNave;

import br.com.voo.model.e.ETipoAviao;
import br.com.voo.model.pessoas.Piloto;

public class Aviao {

    private String prefixo;
    private int numeroAssentos;
    private ETipoAviao tipoAviao;

    public Aviao() {
    }

    public Aviao(String prefixo, int numeroAssentos, ETipoAviao tipoAviao) {
        this.prefixo = prefixo;
        this.numeroAssentos = numeroAssentos;
        this.tipoAviao = tipoAviao;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public int getNumeroAssentos() {
        return numeroAssentos;
    }

    public void setNumeroAssentos(int numeroAssentos) {
        this.numeroAssentos = numeroAssentos;
    }

    public ETipoAviao getTipoAviao() {
        return tipoAviao;
    }

    public void setTipoAviao(ETipoAviao tipoAviao) {
        this.tipoAviao = tipoAviao;
    }
}
