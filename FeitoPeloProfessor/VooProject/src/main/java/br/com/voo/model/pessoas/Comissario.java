package br.com.voo.model.pessoas;

import br.com.voo.model.i.IAeroViario;

import java.time.LocalDateTime;

public class Comissario extends Pessoa implements IAeroViario {

    private final int VALOR_HORA = 100; //snack_case

    private int numeroHorasVoo; //camelCase

    public Comissario() {
    }

    public Comissario(String nome, String login, String senha, String email, LocalDateTime dataAcesso, int numeroHorasVoo) {
        super(nome, login, senha, email, dataAcesso);
        this.numeroHorasVoo = numeroHorasVoo;
    }

    @Override
    public double salario() {
        return numeroHorasVoo * VALOR_HORA - taxaSindical();
    }

    @Override
    public double taxaSindical() {
        return 128.79;
    }

    public int getVALOR_HORA() {
        return VALOR_HORA;
    }

    public int getNumeroHorasVoo() {
        return numeroHorasVoo;
    }

    public void setNumeroHorasVoo(int numeroHorasVoo) {
        this.numeroHorasVoo = numeroHorasVoo;
    }
}
