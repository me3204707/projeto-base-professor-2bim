package br.com.voo.model.pessoas;

import br.com.voo.model.e.ETipoAviao;
import br.com.voo.model.i.IAeroViario;
import br.com.voo.model.i.ITipoAviao;

import java.time.LocalDateTime;

public class Piloto extends Pessoa implements IAeroViario, ITipoAviao {

    private ETipoAviao tipoAviao;
    private double salarioFixo, horasVoo;
    private boolean ehInternacional;

    public Piloto() {
    }

    public Piloto(String nome, String login, String senha,
                  String email, LocalDateTime dataAcesso,
                  ETipoAviao tipoAviao, double salarioFixo,
                  double horasVoo, boolean ehInternacional) {
        super(nome, login, senha, email, dataAcesso);
        this.tipoAviao = tipoAviao;
        this.salarioFixo = salarioFixo;
        this.horasVoo = horasVoo;
        this.ehInternacional = ehInternacional;
    }

    @Override
    public double salario() {
        return (salarioFixo + (BonusTipoAviao()*horasVoo) -taxaSindical())
                * ((ehInternacional)?1.59:1);
    }

    @Override
    public double taxaSindical() {
        return BonusTipoAviao() * 0.05;
    }

    @Override
    public double BonusTipoAviao() {
        if(tipoAviao == ETipoAviao.Boeing){
            return  1500;
        } else if (tipoAviao == ETipoAviao.AirBus) {
            return 2500;
        } else if (tipoAviao == ETipoAviao.Embraer) {
            return 500;
        } else if (tipoAviao == ETipoAviao.Antonov) {
            return 5000;
        } else if (tipoAviao == ETipoAviao.legacy) {
            return 200;
        }
        return 0;
    }

    @Override
    public ETipoAviao getTipoAviao() {
        return tipoAviao;
    }

    public void setTipoAviao(ETipoAviao tipoAviao) {
        this.tipoAviao = tipoAviao;
    }

    public double getSalarioFixo() {
        return salarioFixo;
    }

    public void setSalarioFixo(double salarioFixo) {
        this.salarioFixo = salarioFixo;
    }

    public double getHorasVoo() {
        return horasVoo;
    }

    public void setHorasVoo(double horasVoo) {
        this.horasVoo = horasVoo;
    }

    public boolean isEhInternacional() {
        return ehInternacional;
    }

    public void setEhInternacional(boolean ehInternacional) {
        this.ehInternacional = ehInternacional;
    }
}
