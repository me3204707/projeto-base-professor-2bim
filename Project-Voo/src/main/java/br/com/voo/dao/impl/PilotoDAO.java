package br.com.voo.dao.impl;

import br.com.voo.dao.IGenericDAO;
import br.com.voo.model.pessoas.Piloto;

import java.util.ArrayList;

public class PilotoDAO implements IGenericDAO<Piloto,Integer> {


    @Override
    public void inserir(Piloto obj) {

    }

    @Override
    public void alterar(Piloto obj) {

    }

    @Override
    public void apagar(Piloto obj) {

    }

    @Override
    public Piloto buscar(Integer key) {
        return null;
    }

    @Override
    public ArrayList<Piloto> buscarTOdos() {
        return null;
    }

    @Override
    public int quantidade() {
        return 0;
    }
}
