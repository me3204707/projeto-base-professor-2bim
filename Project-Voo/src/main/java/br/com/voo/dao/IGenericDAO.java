package br.com.voo.dao;


import java.sql.SQLException;
import java.util.ArrayList;

public interface IGenericDAO <C,K>{



    public void inserir(C obj)throws ClassNotFoundException, SQLException;
    public void alterar(C obj)throws ClassNotFoundException, SQLException;
    public void apagar(C obj)throws ClassNotFoundException, SQLException;
    public C buscar(K key)throws ClassNotFoundException, SQLException;
    public ArrayList<C> buscarTOdos  ()throws ClassNotFoundException, SQLException;
    public int quantidade ()throws ClassNotFoundException, SQLException;


}
