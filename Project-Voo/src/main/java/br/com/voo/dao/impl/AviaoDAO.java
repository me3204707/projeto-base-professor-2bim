package br.com.voo.dao.impl;

import br.com.voo.dao.IGenericDAO;
import br.com.voo.model.AeroNave.Aviao;
import br.com.voo.model.e.ETipoAviao;
import br.com.voo.util.connection.ConnectionFactory;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AviaoDAO implements IGenericDAO <Aviao,Integer> {


    @Override
    public void inserir(Aviao obj) throws ClassNotFoundException, SQLException {
        //Conecta
        Connection c = ConnectionFactory.getConnectionMysql();
       try {


           // Preparar SQL
           String sql = "INSERT INTO `BD-Aeroporto`.aviao" +
                   "(prefixo, numero_assentos, tipo)" +
                   "VALUES(?,?,?);";

           // Preparar Execucao

           PreparedStatement pst = c.prepareStatement(sql);
           pst.setString(1, obj.getPrefixo());
           pst.setInt(2, obj.getNumeroAssentos());
           pst.setString(3, obj.getTipoAviao().ToString());


           // executar

           pst.execute();
       }finally {
           c.close();
       }
    }

    @Override
    public void alterar(Aviao obj) throws ClassNotFoundException, SQLException {
        Connection c = ConnectionFactory.getConnectionMysql();
        try{
            String sql ="UPDATE `BD-Aeroporto`.aviao\n" +
                    "SET prefixo=?, numero_assentos=?, tipo-aviao=?\n" +
                    "WHERE id=?;\n";
        }finally {
            c.close();
        }
    }

    @Override
    public void apagar(Aviao obj) throws ClassNotFoundException, SQLException {
        Connection c = ConnectionFactory.getConnectionMysql();
        try{
            String sql ="DELETE FROM viacao.aviao\n" +
                    "WHERE id=?;\n";
        }finally {
            c.close();
        }
    }

    @Override
    public Aviao buscar(Integer key) throws ClassNotFoundException, SQLException {

        Connection c = ConnectionFactory.getConnectionMysql();
        try{
            String sql ="SELECT id, prefixo, numero_assentos, tipo\n" +
                    "FROM viacao.aviao;"+
                    "WHERE id=? \n";


            PreparedStatement pst =c.prepareStatement(sql);
            pst.setInt(1,key);
            ResultSet resultado =pst.executeQuery();

            Aviao av = null;
            if(resultado.next()){
                av = new Aviao(resultado.getInt(1),
                        resultado.getString(2),
                        resultado.getInt(3),
                        ETipoAviao.valueOf(resultado.getString(4))
                        );
            }
        }finally {
            c.close();
        }


        return null;
    }

    @Override
    public ArrayList<Aviao> buscarTOdos() throws ClassNotFoundException, SQLException {


        Connection c = ConnectionFactory.getConnectionMysql();
        try{
            String sql ="SELECT id, prefixo, numero_assentos, tipo\n" +
                    "FROM viacao.aviao;"+
                    "WHERE id=? \n";


            PreparedStatement pst =c.prepareStatement(sql);
            pst.setInt(1,key);
            ResultSet resultado =pst.executeQuery();

            Aviao av = null;
            if(resultado.next()){
                av = new Aviao(resultado.getInt(1),
                        resultado.getString(2),
                        resultado.getInt(3),
                        ETipoAviao.valueOf(resultado.getString(4))
                );
            }
        }finally {
            c.close();
        }


        return null;
    }

    @Override
    public int quantidade() throws ClassNotFoundException, SQLException {
        return 0;
    }
}
