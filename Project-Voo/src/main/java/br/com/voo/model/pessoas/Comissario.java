package br.com.voo.model.pessoas;

import br.com.voo.model.i.IAeroViario;

import java.time.LocalDateTime;

public class Comissario extends Pessoa implements IAeroViario {
    public Comissario(String nome, String login, String senha, String email, LocalDateTime dataAcesso, double salario, int numeroHorasDeVoo) {
        super(nome, login, senha, email, dataAcesso, salario);
        this.numeroHorasDeVoo = numeroHorasDeVoo;
    }

    public Comissario() {
    }

    private int numeroHorasDeVoo;

    private final int VALOR_HORA=100;

    public int getNumeroHorasDeVoo() {
        return numeroHorasDeVoo;
    }

    public void setNumeroHorasDeVoo(int numeroHorasDeVoo) {
        this.numeroHorasDeVoo = numeroHorasDeVoo;
    }

    public int getVALOR_HORA() {
        return VALOR_HORA;
    }

    //Constant
    @Override
    public double salario() {

        return numeroHorasDeVoo*VALOR_HORA;

    }

    @Override
    public void calculaTaxa() {

    }

    @Override
    public double taxaSindical() {
        return 0;
    }
}
