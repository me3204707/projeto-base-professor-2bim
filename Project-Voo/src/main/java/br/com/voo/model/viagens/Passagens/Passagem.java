package br.com.voo.model.viagens.Passagens;

import br.com.voo.model.pessoas.Passageiro;

public class Passagem {

    private int numeroAssento;
    private Passageiro passageiro;

    private double valor;

    public Passagem() {
    }

    public Passagem(int numeroAssento, Passageiro passageiro, double valor) {
        this.numeroAssento = numeroAssento;
        this.passageiro = passageiro;
        this.valor = valor;
    }

    public int getNumeroAssento() {
        return numeroAssento;
    }

    public void setNumeroAssento(int numeroAssento) {
        this.numeroAssento = numeroAssento;
    }

    public Passageiro getPassageiro() {
        return passageiro;
    }

    public void setPassageiro(Passageiro passageiro) {
        this.passageiro = passageiro;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
