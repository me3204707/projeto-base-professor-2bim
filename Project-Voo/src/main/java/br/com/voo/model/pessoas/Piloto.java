package br.com.voo.model.pessoas;

import br.com.voo.model.e.ETipoAviao;
import br.com.voo.model.i.IAeroViario;
import br.com.voo.model.i.ITipoAviao;

import java.time.LocalDateTime;

public class Piloto extends Pessoa implements IAeroViario, ITipoAviao {
    private ETipoAviao tipoAviao;
    private boolean ehInternacional;
    private double salarioFixo,horasVoo;

    public void setTipoAviao(ETipoAviao tipoAviao) {
        this.tipoAviao = tipoAviao;
    }

    public boolean isEhInternacional() {
        return ehInternacional;
    }

    public void setEhInternacional(boolean ehInternacional) {
        this.ehInternacional = ehInternacional;
    }

    public double getSalarioFixo() {
        return salarioFixo;
    }

    public void setSalarioFixo(double salarioFixo) {
        this.salarioFixo = salarioFixo;
    }

    public double getHorasVoo() {
        return horasVoo;
    }

    public void setHorasVoo(double horasVoo) {
        this.horasVoo = horasVoo;
    }

    public Piloto(String nome, String login, String senha, String email, LocalDateTime dataAcesso, double salario, ETipoAviao tipoAviao, boolean ehInternacional, double salarioFixo, double horasVoo) {
        super(nome, login, senha, email, dataAcesso, salario);
        this.tipoAviao = tipoAviao;
        this.ehInternacional = ehInternacional;
        this.salarioFixo = salarioFixo;
        this.horasVoo = horasVoo;
    }

    public Piloto() {
    }

    @Override
    public double salario() {

        return (salarioFixo+(bonusTipoAviao()*horasVoo)- taxaSindical())+((ehInternacional)?1.59:0);
    }

    @Override
    public void calculaTaxa() {

    }

    @Override
    public double taxaSindical() {
        return bonusTipoAviao()*0.05;
    }

    @Override
    public double BonusTipoAviao() {
        return 0;
    }

    @Override
    public ETipoAviao getTipoAviao() {
        return null;
    }
    public  double bonusTipoAviao(){
         double bonus=0;
        if(tipoAviao==ETipoAviao.Boeing){

            return 1500;
        }
        else if(tipoAviao==ETipoAviao.AirBus){
            return 2500;
        }
        else if(tipoAviao==ETipoAviao.Embraer){
            return 500;
        }
        else if(tipoAviao==ETipoAviao.Antonov){
            return 5000;
        }
        else if(tipoAviao==ETipoAviao.Legacy){
            return 200;
        }
        return 0;
    }
}
