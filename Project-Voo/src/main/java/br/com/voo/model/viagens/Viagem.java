package br.com.voo.model.viagens;

import br.com.voo.model.AeroNave.Aviao;
import br.com.voo.model.i.IAeroViario;
import br.com.voo.model.pessoas.Piloto;
import br.com.voo.model.viagens.Passagens.Passagem;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Viagem {

    private String numeroVoo;
    private String origem,destino;
    private LocalDateTime dataViagem;
    private Aviao aviao;
    private Piloto piloto;

    private ArrayList <IAeroViario> tripulacao;

    private ArrayList<Passagem> passagens;

    public Viagem() {
    }

    public Viagem(String numeroVoo, String origem, String destino, LocalDateTime dataViagem, Aviao aviao, Piloto piloto, ArrayList<IAeroViario> tripulacao, ArrayList<Passagem> passagens) {
        this.numeroVoo = numeroVoo;
        this.origem = origem;
        this.destino = destino;
        this.dataViagem = dataViagem;
        this.aviao = aviao;
        this.piloto = piloto;
        this.tripulacao = tripulacao;
        this.passagens = passagens;
    }

    public String getNumeroVoo() {
        return numeroVoo;
    }

    public void setNumeroVoo(String numeroVoo) {
        this.numeroVoo = numeroVoo;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public LocalDateTime getDataViagem() {
        return dataViagem;
    }

    public void setDataViagem(LocalDateTime dataViagem) {
        this.dataViagem = dataViagem;
    }

    public Aviao getAviao() {
        return aviao;
    }

    public void setAviao(Aviao aviao) {
        this.aviao = aviao;
    }

    public Piloto getPiloto() {
        return piloto;
    }

    public void setPiloto(Piloto piloto) {
        this.piloto = piloto;
    }

    public ArrayList<IAeroViario> getTripulacao() {
        return tripulacao;
    }

    public void setTripulacao(ArrayList<IAeroViario> tripulacao) {
        this.tripulacao = tripulacao;
    }

    public ArrayList<Passagem> getPassagens() {
        return passagens;
    }

    public void setPassagens(ArrayList<Passagem> passagens) {
        this.passagens = passagens;
    }
}

