package br.com.voo.model.i;

import br.com.voo.model.e.ETipoAviao;

public interface ITipoAviao {


    public double BonusTipoAviao();

    public ETipoAviao getTipoAviao();


}
