package br.com.voo.model.i;

public interface IAeroViario {
    public void calculaTaxa();
    public double taxaSindical();
}
