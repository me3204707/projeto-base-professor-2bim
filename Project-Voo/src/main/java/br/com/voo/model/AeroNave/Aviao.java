package br.com.voo.model.AeroNave;

import br.com.voo.model.e.ETipoAviao;

public class Aviao
{
    private int idAviao;
    private ETipoAviao tipoAviao;
    private  int numeroAssentos;

    private  String prefixo;

    public ETipoAviao getTipoAviao() {
        return tipoAviao;
    }

    public void setTipoAviao(ETipoAviao tipoAviao) {
        this.tipoAviao = tipoAviao;
    }

    public int getNumeroAssentos() {
        return numeroAssentos;
    }

    public void setNumeroAssentos(int numeroAssentos) {
        this.numeroAssentos = numeroAssentos;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public int getIdAviao() {
        return idAviao;
    }

    public void setIdAviao(int idAviao) {
        this.idAviao = idAviao;
    }

    public Aviao(int idAviao, ETipoAviao tipoAviao, int numeroAssentos, String prefixo) {
        this.idAviao = idAviao;
        this.tipoAviao = tipoAviao;
        this.numeroAssentos = numeroAssentos;
        this.prefixo = prefixo;
    }

    public Aviao() {
    }
}
